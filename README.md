# Vagrant environment for Kubernetes with RKE

## Set up Windows 10 environment

1. Open PowerShell with admin rights
2. Run ```Get-ExecutionPolicy```
3. If ```Restricted```. Run ```Set-ExecutionPolicy AllSigned``` or ```Set-ExecutionPolicy Bypass -Scope Process```
4. Run ```Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))```
5. Install necessary programs:
    - git: ```choco install git```
    - Vagrant: ```choco install vagrant```
    - VirtualBox: ```choco install virtualbox```
6. Clone repository and switch to the directory

## Provision VMs

1. Use the ```config.json``` to configure your environment
2. Run ```vagrant up```

## Check everything is installed and accessible after provisioning

1. Run ```vagrant ssh control-node```
2. Run ```kubectl version --client```
3. Run ```rke --version```
4. Run ```ssh vagrant@192.168.5.3 docker version```

## Deploying the Kubernetes cluster and checking that it is running

1. Run ```rke up --config /vagrant/k8s_cluster/configs/rke-cluster.yml```
2. Run ```kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_rke-cluster.yml version```
3. Run ```kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_rke-cluster.yml cluster-info```

## YAML based deployment of the 'nginx'

```
kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_rke-cluster.yml  \ 
    apply -f /vagrant/k8s_cluster/deployments/nginx.yml 
```

## Step-by-step deployment of the 'Rancher Demo'
 
```
kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_rke-cluster.yml \ 
    create deployment ranger-demo-deployment  \
    --image=superseb/rancher-demo
```

```
kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_rke-cluster.yml \
    expose deployment ranger-demo-deployment \
    --type=NodePort \
    --port=8080 \
    --name rancher-demo-service
```

```
kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_rke-cluster.yml \
    apply -f /vagrant/k8s_cluster/deployments/ingress.yml
```

## Useful commands

- ```kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_k8s-cluster.yml get nodes```
- ```kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_k8s-cluster.yml get pods```
- ```kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_k8s-cluster.yml get deployments```
- ```kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_k8s-cluster.yml get services```
- ```kubectl --kubeconfig /vagrant/k8s_cluster/configs/kube_config_k8s-cluster.yml get ingress```
